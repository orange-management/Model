/**
 * Set message.
 *
 * @param {{delay:int}} data Message data
 *
 * @since 1.0.0
 * @author Dennis Eichhorn <d.eichhorn@oms.com>
 */
var reloadMessage = function (data) {
    setTimeout(function () {
        document.location.reload(true);
    }, parseInt(data.delay));
};
