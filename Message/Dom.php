<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Model\Message;


use phpOMS\Contract\ArrayableInterface;
use phpOMS\Contract\RenderableInterface;

/**
 * Dom class.
 *
 * @category   Modules
 * @package    Model\Message
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Dom implements RenderableInterface, ArrayableInterface
{

    /**
     * Message type.
     *
     * @var string
     * @since 1.0.0
     */
    const TYPE = 'dom';

    /**
     * Selector string.
     *
     * @var string
     * @since 1.0.0
     */
    private $selector = '';

    private $content = '';

    /**
     * Dom actino.
     *
     * @var DomAction
     * @since 1.0.0
     */
    private $action = DomAction::MODIFY;

    /**
     * Delay in ms.
     *
     * @var int
     * @since 1.0.0
     */
    private $delay = 0;

    /**
     * Set DOM content
     *
     * @param string $content DOM Content
     *
     * @since 1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setContent(string $content) 
    {
        $this->content = $content;
    }

    /**
     * Get DOM content
     *
     * @return string
     *
     * @since 1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getContent() : string 
    {
        return $this->content;
    }

    /**
     * Get selector.
     *
     * @return string
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getSelector() : string
    {
        return $this->selector;
    }

    /**
     * Set selector.
     *
     * @param string $selector Selector
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setSelector(string $selector)
    {
        $this->selector = $selector;
    }

    /**
     * Get action.
     *
     * @return DomAction
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getAction() : DomAction
    {
        return $this->action;
    }

    /**
     * Set action.
     *
     * @param DomAction $action action
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setAction(DomAction $action)
    {
        $this->action = $action;
    }

    /**
     * Set delay.
     *
     * @param int $delay Delay in ms
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setDelay(int $delay)
    {
        $this->delay = $delay;
    }

    /**
     * Render message.
     *
     * @return string
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function render() : string
    {
        return $this->__toString();
    }

    /**
     * Stringify.
     *
     * @return string
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __toString()
    {
        return json_encode($this->toArray());
    }

    /**
     * Generate message array.
     *
     * @return array<string, mixed>
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function toArray() : array
    {
        return [
            'type'     => self::TYPE,
            'time'     => $this->delay,
            'selector' => $this->selector,
            'action'   => $this->action,
        ];
    }
}
