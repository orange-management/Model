<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Model\Message;

use phpOMS\Datatypes\Enum;

/**
 * DomAction class.
 *
 * @category   Modules
 * @package    Model\Message
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
abstract class DomAction extends Enum
{
    const CREATE_BEFORE = 0;

    const CREATE_AFTER = 1;

    const DELETE = 2;

    const REPLACE = 3;

    const MODIFY = 4;
    const SHOW = 5;
    const HIDE = 6;
    const ACTIVATE = 7;
    const DEACTIVATE = 8;
}
