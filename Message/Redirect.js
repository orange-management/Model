/**
 * Set message.
 *
 * @param {{delay:int},{url:string}} data Message data
 *
 * @since 1.0.0
 * @author Dennis Eichhorn <d.eichhorn@oms.com>
 */
var redirectMessage = function (data)
{
    setTimeout(function ()
    {
        document.location.href = data.url;
    }, parseInt(data.delay));
};
