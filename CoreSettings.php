<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Model;

use phpOMS\Config\SettingsAbstract;
use phpOMS\DataStorage\Database\Connection\ConnectionAbstract;

/**
 * Core settings class.
 *
 * This is used in order to manage global Framework and Module settings
 *
 * @category   Modules
 * @package    Model
 * @since      1.0.0
 *
 * @todo       : maybe move this \Web
 */
class CoreSettings extends SettingsAbstract
{

    /**
     * Settings table.
     *
     * @var string
     * @since 1.0.0
     */
    protected static $table = 'settings';

    /**
     * Columns.
     *
     * @var string[]
     * @since 1.0.0
     */
    protected static $columns = [
        'settings_id',
        'settings_content',
    ];

    /**
     * Constructor.
     *
     * @param ConnectionAbstract $connection Database connection
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(ConnectionAbstract $connection)
    {
        $this->connection = $connection;
    }
}
