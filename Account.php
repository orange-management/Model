<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Model;

use phpOMS\Auth\Auth;
use phpOMS\DataStorage\Cache\Pool as CachePool;
use phpOMS\DataStorage\Database\Connection\ConnectionAbstract;
use phpOMS\DataStorage\Session\SessionInterface;
use phpOMS\Datatypes\Address;
use phpOMS\Localization\Localization;

/**
 * Account class.
 *
 * @category   Modules
 * @package    Model
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Account extends \phpOMS\Account\Account
{

    /**
     * Database connection.
     *
     * @var ConnectionAbstract
     * @since 1.0.0
     */
    private $connection = null;

    /**
     * Session manager.
     *
     * @var SessionInterface
     * @since 1.0.0
     */
    private $sessionManager = null;

    /**
     * Cache manager.
     *
     * @var CachePool
     * @since 1.0.0
     */
    private $cachePool = null;

    /**
     * Multition cache.
     *
     * @var self[]
     * @since 1.0.0
     */
    private static $instances = [];

    /**
     * Constructor.
     *
     * @param int               $id             Account id
     * @param ConnectionAbstract $connection     Database connection
     * @param SessionInterface   $sessionManager Session manager
     * @param CachePool       $cachePool   Cache manager
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(int $id, ConnectionAbstract $connection, SessionInterface $sessionManager, CachePool $cachePool)
    {
        $this->id             = $id;
        $this->connection     = $connection;
        $this->sessionManager = $sessionManager;
        $this->cachePool   = $cachePool;

        $this->l11n = new Localization();
    }

    /**
     * Multition constructor.
     *
     * @param int               $id             Account id
     * @param ConnectionAbstract $connection     Database connection
     * @param SessionInterface   $sessionManager Session manager
     * @param CachePool       $cachePool   Cache manager
     *
     * @return Account
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public static function getInstance(int $id, ConnectionAbstract $connection, SessionInterface $sessionManager, CachePool $cachePool) : Account
    {
        if (!isset(self::$instances[$id])) {
            self::$instances[$id] = new self($id, $connection, $sessionManager, $cachePool);
        }

        return self::$instances[$id];
    }

    /**
     * Authenticate account.
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function authenticate()
    {
        $this->id = (new Auth($this->connection, $this->sessionManager))->authenticate();
    }

    /**
     * Get localization.
     *
     * @return Localization
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getL11n() : Localization
    {
        return $this->l11n;
    }

    /**
     * Get account id.
     *
     * @return int
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Login account.
     *
     * @param string $user     Login
     * @param string $password Password
     *
     * @return int Login code
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function login($user, $password) : int
    {
        return (new Auth($this->connection, $this->sessionManager))->login($user, $password);
    }
}
